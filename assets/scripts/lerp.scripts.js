const circle = document.getElementById("circle");
let mouseX = window.innerWidth,
  mouseY = window.innerHeight;

let globalId;
let stop = false;

document.body.addEventListener("click", function () {
  circle.classList.toggle("stop");
  if (!stop) {
    repeatOften();
    stop = !stop;
  } else {
    window.cancelAnimationFrame(globalId);
    stop = !stop;
  }
});

function repeatOften() {
  circleClass.x = lerp(circleClass.x, mouseX, 0.1);
  circleClass.y = lerp(circleClass.y, mouseY, 0.1);
  circleClass.update();
  globalId = window.requestAnimationFrame(repeatOften);
}

function lerp(start, end, amt) {
  return (1 - amt) * start + amt * end;
}

let circleClass = {
  x: window.innerWidth / 2,
  y: window.innerHeight / 2,
  w: 100,
  h: 100,
  update: function () {
    l = this.x - this.w / 2;
    t = this.y - this.h / 2;
    circle.setAttribute("style", `transform: translate3d(${l}px, ${t}px, 0)`);
  },
};

function mouseMoving(e) {
  mouseX = e.clientX;
  mouseY = e.clientY;
}


document.body.addEventListener("mousemove", mouseMoving);
